var Stork = (function (window, $) {

    var Stork = {
        version: "",
        prod:[],
        lable: "",
    };

    function loadSupportingFiles(callback) {

        init();
        loadStylesheet("http://direct.akku.ru/plugins/akku-ru/widgets/akkuru.css");
        var params = getWidgetParams();
        getRatingData(params, callback);

    }

    function getWidgetParams() {

        
        if (Stork.prod.length>0) {

            return { prod_id: Stork.prod.join() }
        }
        else {

            var c = location.toString();

            switch (c) {
                case "http://mama1.1gb.ru/2015/12/19/hello/":
                    c = "http://robiton/product/07025";
                    break;

                case "http://mama1.1gb.ru/2015/12/23/second/":
                    c = "http://garin/product/11345";
                    break;

                case "http://bvs.1gb.ru/2015/12/28/test1/":
                    c = "http://robiton/product/07025";
                    break;

            }


            return { codeurl: c }
        }
    }

    function getRatingData(params, callback) {
        $.post("http://direct.akku.ru/tests/market/y50mq",
            params,
            callback,
            "html");
    }

    function drawWidget(h) {
        $('#'+Stork.lable).append(h);
    }

    function loadStylesheet(url) {
        var link = window.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        var entry = window.document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
    }

    function init() {
        var scripts = document.getElementsByTagName('script');
        var id;
        for (var i = 0; i < scripts.length; i++) {
            var scr = scripts[i];
            wd = scr.getAttribute('ru-akku-widget-offers');
            if (wd) {

                eval("var dt=" + wd);

                Stork.version = dt.vers;
                Stork.lable = "y" + dt.vers.replace(/\./g, "") + "mq";
                if (dt.prod) {
                    Stork.prod = dt.prod;
                }

                var div = window.document.createElement("div");
                div.id = Stork.lable;
                scr.parentNode.insertBefore(div, scr);

                break;
            }
        }
    }

    loadSupportingFiles(drawWidget);

    return Stork;
}
)(window,jQuery);

