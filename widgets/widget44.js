/*

<div id="dvy44mq">&nbsp;</div>

<div id="wtb-button">&nbsp;</div>
<script ru-akku-widget-offers="{vers:44}">
(function(d,w,t){var s=d.createElement(t),e=d.getElementsByTagName(t)[0];
s.async=!0;s.src=w+'?id=sct'+Math.random().toString(16).slice(2);e.parentNode.insertBefore(s,e)})
(document,'https://direct.akku.ru/plugins/akku-ru/widgets/widget44.js','script')
</script>


*/

var theOffers = theOffers || (function (w, $) {

    //constants:
    var VERSION = 44;
    var BASE_URL = "https://direct.akku.ru/market/market/";

    //private:
        
    var  delSubscribe = function(cook) {
        var subscribe = $.cookie('_akku_subscribe');
        if (subscribe !== null) {
            var all = subscribe.split(',');
            subscribe = '';
            for (var i = 0; i < all.length; i++) {
                if (Number(all[i]) == cook) {
                    continue;
                }
                subscribe += ',' + all[i];
            }
            if (subscribe.length > 1) setSubscribe(subscribe.substring(1));
            else setSubscribe(null);
        }
    }
   
    var setSubscribe = function(cookies)
    {
        $.cookie('_akku_subscribe', cookies, {
            expires: 36000,
            path: '/',
        });
    }

    var getSubscribe = function()
    {
        var subscribe = $.cookie('_akku_subscribe');
        return { "akku-subscribe": subscribe === null ? "" : subscribe }
    }

    var clearMessagesHandler = null;

    var XXX = function ()
    {
        if (clearMessagesHandler !== null) {
            clearTimeout(clearMessagesHandler);
            clearMessagesHandler = null;
        }

        $("#dvy44mq > div > span").remove();
    }

    var hideMessages = function ()
    {
        if (clearMessagesHandler !== null)
        {
            clearTimeout(clearMessagesHandler);
            clearMessagesHandler = null;
        }
        $("#dvy44mq > div > span").removeClass("visible")
    }


    var widget = widget || {

        label: function (pref) {
            return (pref ? pref : "ru_akku_tmpid_") + Math.random().toString(16).slice(2)
        }

        /*
        ,test: function (cook)
        {
        //    delSubscribe(cook);
        }
        
        tryeval: function (expr, prop) {
            try {
                eval("var dt = " + expr);
            }
            catch (e) {
            }
            return typeof dt === "undefined" || typeof dt[prop] === "undefined" ? null : dt[prop];
        }
        ,*/

        
        // Public:
        
        , SubscribeNoOffers: function (site, geo_id, product) {
            $.ajax({
                type: "POST",
                url: BASE_URL +"y81noOffers",
                data: {
                    product: product,
                    geo_id: geo_id,
                    site: site,
                    EMail: $("#NoOffers_EMail")[0].value
                },
                success: function (h, s, x) {

                    var asb = x.getResponseHeader("akku-subscribe");
                    if (asb === null) { // ���� ���� �����
                        XXX();
//                        clearMessages();
                        $("#dvy44mq > div > p").after(h);
                        $("#dvy44mq > div > span").addClass('visible');
                        clearMessagesHandler = setTimeout(hideMessages, 5000);
                    }
                    else {
                        setSubscribe(asb);
                        var div = $("#dvy44mq");
                        div.empty();
                        div.append(h);
                    }
                }
                , dataType: "html"
                , headers: getSubscribe()
                //,  xhrFields: { withCredentials: true } -- cool
            });

        }

        , UnsubscribeNoOffers: function (cookie) {

            $.ajax({
                type: "POST"
                , url: BASE_URL + "y81noOffersUnsubscribe"
                , data: {
                    cookie: cookie
                }
                , success: function (h) {
                    delSubscribe(cookie);
                    var div = $("#dvy44mq");
                    div.empty();
                    div.append(h);
                }
                , dataType: "html"
                , headers: getSubscribe()
            })
        }
    }

    $.ajax({

        type: "POST",
        url: BASE_URL + "y81mq",
        data: {
            vers: VERSION
        }, success: function (h) {
            $("#dvy44mq").append(h);

            if ($("#NoOffers_EMail").length === 0)
                $('#wtb-button').append($('#ya-summary').css('visibility', 'visible'));
            else
                $('#wtb-button').hide();
        }
        , dataType: "html"
        , headers: getSubscribe()
    })

    return widget;

})(window, jQuery);

