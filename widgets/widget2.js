var theOffers = theOffers || (function (window, undefined) {


    var widget = widget || {

        test: function (s) {
            console.log(s);
        }
        ,
        tryeval: function (expr,prop) {
            try{
                eval("var dt = " + expr);
            }
            catch(e) {
            }
            return typeof dt === "undefined" || typeof dt[prop] === "undefined" ? null : dt[prop];
        }
        ,
        label: function (pref) {
            return (pref? pref:"ru_akku_tmpid_")+Math.random().toString(16).slice(2)
        }
        ,
        jquery: "none"
        , image: 0
    }

    function loadStylesheet(url) {
        var link = window.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        var entry = window.document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
    }

    function load_jQuery(src) {
        if (widget.jquery=="none") {
            var jq = document.createElement('script');
            jq.type = 'text/javascript';
            jq.src = src;
            document.getElementsByTagName('head')[0].appendChild(jq);
            widget.jquery = "loading";
        }
    }

    function init() {

        var scripts = document.getElementsByTagName('script');
        var id;
        for (var i = 0; i < scripts.length; i++) {
            var scr = scripts[i];
            wd = scr.getAttribute('ru-akku-widget-offers');
            if (wd) {
                scr.id = widget.label();

                eval("var dt=" + wd);
                if (dt.vers)
                    widget.version = dt.vers;
                if (dt.image>=0)
                    widget.image = dt.image;

                break;
            }
        }
      //  widget.label = Math.random().toString(16).slice(2);
        is_jQueryReady(function () {

            if (widget.version > 50) {
                loadStylesheet("https://direct.akku.ru/plugins/akku-ru/widgets/akkuru.css");
                isCssReady(widget.refresh);
            }
            else
            {
                widget.refresh();
            }
            /*
            function () {
                widget.$('[ru-akku-widget-offers-prod]').each(function () { widget.full(this) });
            });
            */
        });

    }

    widget.refresh = function()
    {
        widget.$('[ru-akku-widget-offers-prod]').each(function () { widget.full(this) });
    }

    widget.full = function (el) {

        var div = widget.$(el);

        if (widget.version < 50) {
            div.append('<div id="dvy44mq">&nbsp;</div> \
<div id="wtb-button">&nbsp;</div>');

            div = widget.$("#dvy44mq")

        }
        var prod = this.tryeval(div.attr('ru-akku-widget-offers-prod'), 'prod');
        var ref = null;
        if (prod === null)
        {
            var elem = this.tryeval(div.attr('ru-akku-widget-offers-prod'), 'elem');
            var attr = this.tryeval(div.attr('ru-akku-widget-offers-prod'), 'refer2');
            ref = widget.$(elem).attr(attr);
    //        console.log(ref);
        }


            widget.$.post("https://direct.akku.ru/market/market/y80mq",
               {
                   vers: this.version,
                   prod_id: prod !== null ? prod.join() : ""
                   , image: this.image
                   , refer2 : ref
               },
               function (htm) {
                   div.append(htm)

                   if (widget.version < 50) {
                       widget.$('#wtb-button').append(widget.$('#ya-summary').css('visibility', 'visible'));
                   }
               },
               "html");
     /*   }
        else
        {
            console.log(widget.version);

     <div id="dvy44mq">&nbsp;</div>

<div id="wtb-button">&nbsp;</div>
<script>(function(s,q){
q.post(s,function(h)
	{
		q("#dvy44mq").append(h);
		q('#wtb-button').append(q('#ya-summary').css('visibility','visible'));
	},
	"html")
})("http://direct.akku.ru/tests/market/y60mq",jQuery)
</script>       

            widget.$.post("http://direct.akku.ru/tests/market/y70mq",
            { vers: this.version}, function (h)
            {
                div.append(h)
          //      widget.$("#dvy44mq").append(h);
          //      widget.$('#wtb-button').append(widget.$('#ya-summary').css('visibility', 'visible'));
                },
                    "html")

        }
*/
    }

    widget.addProducts = function (elem,prod) {

        var div = window.document.createElement("div");
        div.setAttribute('ru-akku-widget-offers-prod',"{prod:["+prod.join()+"]}");
        elem[0].parentNode.insertBefore(div, elem[0]);

        this.full(div);

    }

    function is_jQueryReady(callback) {
        (function poll() {
            try
            {
                widget.$ = jQuery.noConflict(true);
                widget.jquery = widget.$().jquery;
                callback();
            }
            catch(e)
            {
                load_jQuery("https://direct.akku.ru/plugins/akku-ru/widgets/jquery-2.2.0.min.js");
                setTimeout(poll, 50);
            }
        })();
    }

    function isCssReady(callback) {
        var testElem = document.createElement('span');
        testElem.id = 'akku-ready';
        testElem.style = 'color: #fff';
        var entry = document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(testElem, entry);
        (function poll() {
            var node = document.getElementById('akku-ready');
            var value;
            if (window.getComputedStyle) {
                value = document.defaultView
                .getComputedStyle(testElem, null)
                .getPropertyValue('color');
            }
            else if (node.currentStyle) {
                value = node.currentStyle.color;
            }
            if (value && value === 'rgb(186, 218, 85)' || value.toLowerCase() === '#bada55')
            {
                callback();
            } else {
                setTimeout(poll, 50);
            }
        })();
    }


    //    isCssReady(init);
    init();

    return widget;

})(window);

//Stork.$ = Stork.jQuery = jQuery.noConflict(true);
/*
var Stork = Stork || (function (window, $) {

    var Stork = {
        version: "",
        prod: [],
        lable: "",

    };

    Stork.test = function (s) {
        console.log(s);
    }

    function loadSupportingFiles(callback) {

        init();
        loadStylesheet("http://direct.akku.ru/plugins/akku-ru/widgets/akkuru.css");
        var params = getWidgetParams();
        getRatingData(params, callback);

    }

    function getWidgetParams() {


        if (Stork.prod.length > 0) {

            return { prod_id: Stork.prod.join() }
        }
        else {

            var c = location.toString();

            switch (c) {
                case "http://mama1.1gb.ru/2015/12/19/hello/":
                    c = "http://robiton/product/07025";
                    break;

                case "http://mama1.1gb.ru/2015/12/23/second/":
                    c = "http://garin/product/11345";
                    break;

                case "http://bvs.1gb.ru/2015/12/28/test1/":
                    c = "http://robiton/product/07025";
                    break;

            }


            return { codeurl: c }
        }
    }

    function getRatingData(params, callback) {
        $.post("http://direct.akku.ru/tests/market/y50mq",
            params,
            callback,
            "html");
    }

    function drawWidget(h) {
        $('#' + Stork.lable).append(h);
    }

    function loadStylesheet(url) {
        var link = window.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        var entry = window.document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
    }

    function init() {
        Stork.test("Init");
        var scripts = document.getElementsByTagName('script');
        var id;
        for (var i = 0; i < scripts.length; i++) {
            var scr = scripts[i];
            wd = scr.getAttribute('ru-akku-widget-offers');
            if (wd) {

                eval("var dt=" + wd);

                Stork.version = dt.vers;
                Stork.lable = "y" + dt.vers.replace(/\./g, "") + "mq";
                if (dt.prod) {
                    Stork.prod = dt.prod;
                }

                var div = window.document.createElement("div");
                div.id = Stork.lable;
                scr.parentNode.insertBefore(div, scr);

                break;
            }
        }
    }

    loadSupportingFiles(drawWidget);

    return Stork;
})(window, jQuery);
*/