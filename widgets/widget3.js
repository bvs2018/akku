﻿var theOffers = theOffers || (function (window, undefined) {

    var widget = widget || {

        test: function (s) {
            console.log(s);
        }
        ,
        tryeval: function (expr,prop) {
            try{
                eval("var dt = " + expr);
            }
            catch(e) {
            }
            return typeof dt === "undefined" || typeof dt[prop] === "undefined" ? null : dt[prop];
        }
        ,
        label: function (pref) {
            return (pref? pref:"ru_akku_tmpid_")+Math.random().toString(16).slice(2)
        }
        ,
        jquery: "none"
        , image: 1
    }

    function loadStylesheet(url) {
        var link = window.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        var entry = window.document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
    }

    function load_jQuery(src) {
        if (widget.jquery=="none") {
            var jq = document.createElement('script');
            jq.type = 'text/javascript';
            jq.src = src;
            document.getElementsByTagName('head')[0].appendChild(jq);
            widget.jquery = "loading";
        }
    }

    function init() {

        var scripts = document.getElementsByTagName('script');
        var id;
        for (var i = 0; i < scripts.length; i++) {
            var scr = scripts[i];
            wd = scr.getAttribute('ru-akku-widget-offers');
            if (wd) {
                scr.id = widget.label();

                eval("var dt=" + wd);
                if (dt.vers)
                    widget.version = dt.vers;
                if (dt.image>=0)
                    widget.image = dt.image;

                break;
            }
        }
      //  widget.label = Math.random().toString(16).slice(2);
        is_jQueryReady(function () {


            loadStylesheet("http://direct.akku.ru/plugins/akku-ru/widgets/akkuru.css");

            isCssReady(widget.refresh);

                /*
                function () {
                widget.$('[ru-akku-widget-offers-prod]').each(function () { widget.full(this) });
            });
            */
        });

    }

    widget.refresh = function()
    {
        widget.$('[ru-akku-widget-offers-prod]').each(function () { widget.full(this) });
    }

    widget.full = function (el) {

        var x = widget.$(el);
        var prod = this.tryeval(x.attr('ru-akku-widget-offers-prod'),'prod');
        widget.$.post("http://direct.akku.ru/tests/market/y51mq",
           {
               prod_id: prod !== null ? prod.join() : ""
               , image: this.image
           },
           function (Model) {
               //console.log(Model);

               x.append('<div class="page_container" />');
               x = x.children();
               if (Model.length > 0) x.append('<h5>Где купить ' + Model[0].brief + '</h5');
               widget.$.each(Model, function (key, mod)
               {
                   if (mod.image) x.append('<img src="http://direct.akku.ru/tests/market/ImageProd?prod=' + mod.prod + '" />');
                   var h = "";
                   widget.$.each(mod.prodUrls, function (key, url) {
                       h += '<tr class="header_row">\
<td colspan="5" class="header_cell">\
<h2 class="table_header">'+ mod.productName + '</h2>\
</td>\
</tr>'
                   });

                   var b = "";
                   widget.$.each(mod.items, function (key2, item) {
                       b +=
                    '<tr class="' + (item.itemPos % 2 == 0 ? "data_odd" : "data_even") + '">\
<td class="data_cell akku_shop"><a target="_blank" rel="nofollow" href="'+ item.url + '"> <span style="white-space:nowrap;font-weight:bold">' + item.name + '</span>  <span class="ya-name-city" style="white-space:nowrap">(' + item.city + ')</span></a></td>\
<td class="data_cell akku_delivery" colspan="'+ item.delicol + '">' + item.delivery + '<div class="qlabs_tooltip">i<span><strong>Подробности:</strong>' + item.full + '</span></div> </td>\
<td class="data_cell akku_pickup" style="display:'+ item.delidisp + '">' + item.pickup + '</td>\
<td class="data_cell akku_price">'+ item.price + '</td>\
<td class="data_cell akku_toshop">\
<div class="grid_button"><a target="_blank" rel="nofollow" href="'+ item.url + '">В магазин</a></div>\
<div class="grid_button akku_tel">                           <a href="tel://+79039688945">Позвонить</a> </div>\
</td></tr>'
                   });

                   x.append('<table class="qlabs_grid_container"><thead>' + h + '</thead><tbody class="data_container">\
<tr class="subheader_row">\
<td class="subheader_cell">Магазин</td>\
<td class="subheader_cell">Доставка</td>\
<td class="subheader_cell">Самовывоз<div class="qlabs_tooltip">i<span><strong>Что это?</strong>возможность предварительно заказать данный товар и забрать его в магазине или пункте выдачи заказов.Это возможность сэкономить и отсутствие необходимости ждать курьера</span></div></td>'+
       (mod.packs > 1 ? '<td class="subheader_cell ">Цена / ' + mod.packs + ' шт.</td>' : '<td class="subheader_cell ">Цена</td>') +
       '<td class="subheader_cell">&nbsp;</td></tr>' + b + '</tbody></table>');

               });

               if (Model.length > 0)
                   x.append('<div style="float:right" class="akku-ym-link-content">\
<span>Данные</span>\
<a rel="nofollow" href="http://market.yandex.ru">Яндекс.Маркет</a>\
</div>');


           },
           "json");
    }

    widget.addProducts = function (elem,prod) {

        var div = window.document.createElement("div");
        div.setAttribute('ru-akku-widget-offers-prod',"{prod:["+prod.join()+"]}");
        elem[0].parentNode.insertBefore(div, elem[0]);

        this.full(div);

    }

    function is_jQueryReady(callback) {
        (function poll() {
            try
            {
                widget.$ = jQuery.noConflict(true);
                widget.jquery = widget.$().jquery;
                callback();
            }
            catch(e)
            {
                load_jQuery("http://direct.akku.ru/plugins/akku-ru/widgets/jquery-2.2.0.min.js");
                setTimeout(poll, 50);
            }
        })();
    }

    function isCssReady(callback) {
        var testElem = document.createElement('span');
        testElem.id = 'akku-ready';
        testElem.style = 'color: #fff';
        var entry = document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(testElem, entry);
        (function poll() {
            var node = document.getElementById('akku-ready');
            var value;
            if (window.getComputedStyle) {
                value = document.defaultView
                .getComputedStyle(testElem, null)
                .getPropertyValue('color');
            }
            else if (node.currentStyle) {
                value = node.currentStyle.color;
            }
            if (value && value === 'rgb(186, 218, 85)' || value.toLowerCase() === '#bada55')
            {
                callback();
            } else {
                setTimeout(poll, 50);
            }
        })();
    }


    //    isCssReady(init);
    init();

    return widget;

})(window);

//Stork.$ = Stork.jQuery = jQuery.noConflict(true);
/*
var Stork = Stork || (function (window, $) {

    var Stork = {
        version: "",
        prod: [],
        lable: "",

    };

    Stork.test = function (s) {
        console.log(s);
    }

    function loadSupportingFiles(callback) {

        init();
        loadStylesheet("http://direct.akku.ru/plugins/akku-ru/widgets/akkuru.css");
        var params = getWidgetParams();
        getRatingData(params, callback);

    }

    function getWidgetParams() {


        if (Stork.prod.length > 0) {

            return { prod_id: Stork.prod.join() }
        }
        else {

            var c = location.toString();

            switch (c) {
                case "http://mama1.1gb.ru/2015/12/19/hello/":
                    c = "http://robiton/product/07025";
                    break;

                case "http://mama1.1gb.ru/2015/12/23/second/":
                    c = "http://garin/product/11345";
                    break;

                case "http://bvs.1gb.ru/2015/12/28/test1/":
                    c = "http://robiton/product/07025";
                    break;

            }


            return { codeurl: c }
        }
    }

    function getRatingData(params, callback) {
        $.post("http://direct.akku.ru/tests/market/y50mq",
            params,
            callback,
            "html");
    }

    function drawWidget(h) {
        $('#' + Stork.lable).append(h);
    }

    function loadStylesheet(url) {
        var link = window.document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = url;
        var entry = window.document.getElementsByTagName('script')[0];
        entry.parentNode.insertBefore(link, entry);
    }

    function init() {
        Stork.test("Init");
        var scripts = document.getElementsByTagName('script');
        var id;
        for (var i = 0; i < scripts.length; i++) {
            var scr = scripts[i];
            wd = scr.getAttribute('ru-akku-widget-offers');
            if (wd) {

                eval("var dt=" + wd);

                Stork.version = dt.vers;
                Stork.lable = "y" + dt.vers.replace(/\./g, "") + "mq";
                if (dt.prod) {
                    Stork.prod = dt.prod;
                }

                var div = window.document.createElement("div");
                div.id = Stork.lable;
                scr.parentNode.insertBefore(div, scr);

                break;
            }
        }
    }

    loadSupportingFiles(drawWidget);

    return Stork;
})(window, jQuery);
*/